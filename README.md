# IMQ:

1. Messaging Queue system

### 1. Set up server

#### 1.1 Installation

Run `npm install`

#### 1.2 Building Server

1. Go to IMQ/Server
2. Run `npm run build`

#### 1.3 Running Server

1. Run `npm link`
2. Run `connectIMQserver`

### 2. Set up client

#### 2.1 Installation

Run `npm install`

#### 2.2 Building Client

1. Go to IMQ/Client
2. Run `npm run build`

#### 2.3 Running Server

1. Run `npm link`
2. Run any of the following commands:

- `IMQconnect register_subscriber <username> <pasword>`
- `IMQconnect register_publisher <username> <pasword>`
- `IMQconnect login_publisher <username> <pasword>`
- `IMQconnect login_subscriber <username> <pasword>`
