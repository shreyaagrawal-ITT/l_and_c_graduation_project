import Database from '../Database';
import { IMQdatabaseName, IMQtopicsCollection, IMQmessageCollection } from '../../Constants.js';
import IMQprotocol from '../../../Protocol/IMQProtocol';
import IMQmessage from '../../Request/IMQmessage';
import QueueHandler from '../../Queue/QueueHandler';

export default class MessageManager {
	queueHandler = new QueueHandler();
	constructor() {}

	public publishMessage = (request: IMQprotocol) => {
		let database = new Database();
		const message: IMQmessage = {
			data: JSON.parse(request.data).message,
			createdTime: new Date().toString(),
			expiredTime: new Date(new Date().getTime() + 5 * 60000).toString()
		};
		const objectForQueueAndDb = {
			topic: JSON.parse(request.data).topic,
			message: [message]
		};
		this.queueHandler.pushMessagesToQueue(objectForQueueAndDb);
		return new Promise((resolve, reject) => {
			database.connectDb(IMQdatabaseName).then(() => {
				database
					.find(IMQmessageCollection, { topic: JSON.parse(request.data).topic })
					.then((response) => {
						if (response.length != 0) {
						} else {
							database.insert(IMQmessageCollection, objectForQueueAndDb).then((response: any) => {
								if (response.insertedCount != 0) {
									resolve(JSON.stringify(request));
								} else {
									request.data = 'MESSAGE NOT PUBLISHED';
									resolve(JSON.stringify(request));
								}
							});
						}
					})
					.catch((error) => {
						reject(error);
					});
			});
		});
	};

	public pushMessageToExistingCollection() {}
	public pullMessage = (request: IMQprotocol, username: string) => {
		let database = new Database();
		return new Promise((resolve, reject) => {
			database.connectDb(IMQdatabaseName).then(() => {
				database
					.find(IMQmessageCollection, { topic: request.data })
					.then((response) => {
						if (response.length != 0) {
							let messagelength = response[0].message.length;
							request.data = JSON.stringify(response[0].message);
							database.find(IMQtopicsCollection, { topic: request.data, subscriber: { username: username } }).then(() => {
								database.update(
									IMQtopicsCollection,
									{ topic: request.data, subscriber: { username: username } },
									{
										$push: {
											subscriber: {
												lastReadMessageId: messagelength
											}
										}
									}
								);
							});
							resolve(JSON.stringify(request));
						} else {
							request.data = 'Nothing is published on this topic';
							resolve(JSON.stringify(request));
						}
					})
					.catch((error) => {
						reject(JSON.stringify(error));
					});
			});
		});
	};
}
