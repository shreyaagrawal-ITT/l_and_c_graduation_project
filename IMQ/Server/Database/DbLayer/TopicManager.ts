import Database from '../Database';
import { IMQdatabaseName, IMQtopicsCollection } from '../../Constants';
import IMQprotocol from '../../../Protocol/IMQProtocol';
import IMQmessage from '../../Request/IMQmessage';
import { ClientRoles } from '../../../Protocol/Enum';

interface QueueTopic {
	topic: string;
	message: IMQmessage;
}

export default class TopicManager {
	topics = ['books', 'sports', 'movies', 'abc'];

	constructor() {}

	public addTopics = () => {
		let database = new Database();
		return new Promise((resolve, reject) => {
			this.topics.forEach((topic) => {
				database.connectDb(IMQdatabaseName).then(() => {
					database
						.find(IMQtopicsCollection, { topic: topic })
						.then((response) => {
							if (response.length == 0) {
								database.insert(IMQtopicsCollection, { topic: topic, subscriber: [], publisher: [] });
								resolve('TOPICS ADDED');
							} else {
								resolve('TOPICS ADDED');
							}
						})
						.catch((error) => reject(error));
				});
			});
		});
	};

	public connectToTopic(request: IMQprotocol, username: string) {
		let database = new Database();
		return new Promise((resolve, reject) => {
			this.addTopics().then(() => {
				database.connectDb(IMQdatabaseName).then(() => {
					if (request.role == ClientRoles.publisher) {
						database.find(IMQtopicsCollection, { topic: request.data, 'publisher.username': username }).then((response) => {
							if (response.length == 0) {
								database
									.update(
										IMQtopicsCollection,
										{ topic: request.data },
										{
											$push: {
												publisher: {
													username: username
												}
											}
										}
									)
									.then(() => {
										const isTopicValid = this.verifyTopic(request.data);
										if (isTopicValid) {
											resolve(JSON.stringify(request));
										} else {
											request.data = null;
											reject(JSON.stringify(request));
										}
									});
							}
						});
					} else if (request.role == ClientRoles.subscriber) {
						database.find(IMQtopicsCollection, { topic: request.data, 'subscriber.username': username }).then((response) => {
							if (response.length == 0) {
								database
									.update(
										IMQtopicsCollection,
										{ topic: request.data },
										{
											$push: {
												subscriber: {
													lastReadMessageId: 0,
													username: username
												}
											}
										}
									)
									.then(() => {
										const isTopicValid = this.verifyTopic(request.data);
										if (isTopicValid) {
											resolve(JSON.stringify(request));
										} else {
											request.data = null;
											reject(JSON.stringify(request));
										}
									});
							}
						});
					}
				});
			});
		});
	}

	public verifyTopic(topicName: string) {
		const isTopicRegistered = this.topics.includes(topicName);
		return isTopicRegistered;
	}
}
