import Database from '../Database';
import { IMQclientsCollection, IMQdatabaseName } from '../../Constants.js';
import IClientDetails from '../Interface/IClientDetails';
import IMQprotocol from '../../../Protocol/IMQProtocol';

export default class RegistrationManager {
	constructor() {}

	public registerClients = (request: IMQprotocol) => {
		let database = new Database();
		let document: IClientDetails = { username: JSON.parse(request.data).username, password: JSON.parse(request.data).password, role: request.role };
		return new Promise((resolve, reject) => {
			database
				.connectDb(IMQdatabaseName)
				.then(() => database.find(IMQclientsCollection, document))
				.then((response) => {
					if (response.length == 0) {
						database.insert(IMQclientsCollection, document).then((response) => {
							if (response.insertOne != 0) {
								request.data = 'REGISTERED';
								resolve(JSON.stringify(request));
							} else {
								request.data = 'NOT REGISTERED';
								reject(JSON.stringify(request));
							}
						});
					} else {
						request.data = 'REGISTERED';
						reject(JSON.stringify(request));
					}
				})
				.catch((error) => reject(error));
		});
	};

	public loginClients = (request: IMQprotocol) => {
		let database = new Database();
		let document: IClientDetails = { username: JSON.parse(request.data).username, password: JSON.parse(request.data).password, role: request.role };
		return new Promise((resolve, reject) => {
			database.connectDb(IMQdatabaseName).then(() => {
				database
					.find(IMQclientsCollection, document)
					.then((response) => {
						database.closeConnection();
						if (response.length != 0) {
							request.data = 'LOGGED IN';
							resolve(JSON.stringify(request));
						} else {
							request.data = 'NOT REGISTERED';
							reject(JSON.stringify(request));
						}
					})
					.catch((error) => reject(error));
			});
		});
	};
}
