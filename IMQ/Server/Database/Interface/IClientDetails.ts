export default interface IClientDetails {
	username: string;
	password: string;
	role: string;
}
