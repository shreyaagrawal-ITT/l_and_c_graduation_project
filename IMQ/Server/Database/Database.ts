import * as mongodb from 'mongodb';
import { url } from '../Constants';

export default class Database {
	public mongoClient;
	private db;
	constructor() {}

	public async connectDb(dataBaseName: string) {
		this.mongoClient = await new mongodb.MongoClient(url, { useUnifiedTopology: true });
		await this.mongoClient.connect();
		this.db = await this.mongoClient.db(dataBaseName);
	}

	public async insert(collectionName: string, document: any) {
		const collection = this.db.collection(collectionName);
		return await collection.insertOne(document);
	}

	public async find(collectionName: string, document: any) {
		const collection = this.db.collection(collectionName);
		return await collection.find(document).toArray();
	}

	public async update(collectionName, query, options) {
		const collection = this.db.collection(collectionName);
		await collection.updateOne(query, options);
	}

	public async closeConnection() {
		await this.mongoClient.close();
	}
}
