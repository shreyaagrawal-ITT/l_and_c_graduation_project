import * as net from 'net';
import MongoDbConnection from '../Database/Database.js';
import IMQprotocol from '../../Protocol/IMQProtocol';
import RequestHandler from '../Request/RequestHandler';
import { Actions } from '../../Protocol/Enum';
import { Logger } from 'mongodb';

const connection = new MongoDbConnection();

export default class ServerEventHandler {
	private serverSocket: net.Socket;
	private requestHandler: RequestHandler;
	private userName: string;

	constructor(socket: net.Socket) {
		this.serverSocket = socket;
		this.requestHandler = new RequestHandler(this.serverSocket);
		this.serverListner();
	}

	public serverListner() {
		this.serverSocket.on('data', (data) => {
			let request: IMQprotocol = JSON.parse(data.toString('utf-8'));

			switch (request.action) {
				case Actions.register:
					this.userName = JSON.parse(request.data).username;
					this.requestHandler.register(request);
					break;

				case Actions.login:
					this.userName = JSON.parse(request.data).username;
					this.requestHandler.login(request);

					break;

				case Actions.connectTopic:
					this.requestHandler.connectTopic(request, this.userName);
					break;

				case Actions.push:
					this.requestHandler.pushMessage(request);
					break;

				case Actions.pull:
					this.requestHandler.pullMessages(request, this.userName);
			}
		});

		this.serverSocket.on('error', () => {
			console.log('An error occured');
		});
	}
}
