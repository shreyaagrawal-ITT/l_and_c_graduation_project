#!/usr/bin/env node
import ServerEventHandler from './ServerEventHandler.js';
import MongoDbConnection from '../Database/Database.js';
import { PORT } from '../Constants.js';
import * as net from 'net';

class Connection {
	private server;
	connection = new MongoDbConnection();
	constructor() {
		this.connectToClient();
		this.connectServer(PORT);
	}

	connectToClient() {
		this.server = net.createServer((socket: any) => {
			const eventHandler = new ServerEventHandler(socket);
		});
	}

	connectServer(serverPort: number) {
		this.server.listen(serverPort);
	}
}
let connection = new Connection();
