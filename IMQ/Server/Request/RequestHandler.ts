import RegistrationManager from '../Database/DbLayer/UserManager';
import TopicManager from '../Database/DbLayer/TopicManager';
import MessageManager from '../Database/DbLayer/MessageManager';
import * as net from 'net';
import IMQprotocol from '../../Protocol/IMQProtocol';

export default class RequestHandler {
	private serverSocket: net.Socket;
	registrationManager = new RegistrationManager();
	topicManager = new TopicManager();
	messageManager = new MessageManager();

	constructor(socket: net.Socket) {
		this.serverSocket = socket;
	}

	public async register(request: IMQprotocol) {
		await this.registrationManager
			.registerClients(request)
			.then((response) => {
				this.serverSocket.write(JSON.stringify(response));
			})
			.catch((error) => {
				this.serverSocket.write(JSON.stringify(error));
			});
	}

	public async login(request: IMQprotocol) {
		await this.registrationManager
			.loginClients(request)
			.then((response) => {
				this.serverSocket.write(JSON.stringify(response));
			})
			.catch((error) => {
				this.serverSocket.write(JSON.stringify(error));
			});
	}

	public async connectTopic(request: IMQprotocol, username: string) {
		await this.topicManager
			.connectToTopic(request, username)
			.then((response) => {
				this.serverSocket.write(JSON.stringify(response));
			})
			.catch((error) => {
				this.serverSocket.write(JSON.stringify(error));
			});
	}

	public async pushMessage(request: IMQprotocol) {
		await this.messageManager.publishMessage(request).then((response) => {
			this.serverSocket.write(JSON.stringify(response));
		});
	}

	public async pullMessages(request: IMQprotocol, username: string) {
		await this.messageManager.pullMessage(request, username).then((response) => {
			this.serverSocket.write(JSON.stringify(response));
		});
	}
}
