export default interface IMQmessage {
	data: string;
	createdTime: string;
	expiredTime: string;
}
