export const PORT = 59898;

export const url = 'mongodb://localhost:27017';

export const IMQclientsCollection = 'IMQclientDetails';

export const IMQdatabaseName = 'IMQdatabase';

export const IMQtopicsCollection = 'IMQtopics';

export const IMQmessageCollection = 'IMQmessages';
