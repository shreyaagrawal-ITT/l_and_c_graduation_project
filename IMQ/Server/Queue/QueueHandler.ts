import IQueueData from './IQueueData';

export default class QueueHandler {
	queues: IQueueData[] = [];

	public pushMessagesToQueue = async (element: IQueueData) => {
		if (this.queues.length == 0) {
			this.queues.push(element);
		} else {
			let isQueueExists = this.isQueueExistsForTopic(element.topic);
			if (isQueueExists) {
				this.queues.forEach((queue) => {
					if (queue.topic.includes(element.topic)) {
						queue.message.push(element.message[0]);
					}
				});
			} else {
				this.queues.push(element);
			}
		}
	};

	public isQueueExistsForTopic = (topicName: string): boolean => {
		let isQueueExists = false;

		this.queues.forEach((queue) => {
			if (queue.topic.includes(topicName)) {
				isQueueExists = true;
			}
		});
		return isQueueExists;
	};
}
