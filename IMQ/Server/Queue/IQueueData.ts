import IMQmessage from '../Request/IMQmessage';

export default interface QueueData {
	topic: string;
	message: IMQmessage[];
}
