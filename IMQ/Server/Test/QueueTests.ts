import { assert } from 'chai';
import * as mocha from 'mocha';
import QueueHandler from '../Queue/QueueHandler';

describe('topic db layer tests for get messages', () => {
	let queueHandler: QueueHandler;
	beforeEach(() => {
		queueHandler = new QueueHandler();
	});

	it('should not find queue for xyz topic', (done) => {
		let response = queueHandler.isQueueExistsForTopic('xyz');
		assert.equal(false, response);
		done();
	});
});
