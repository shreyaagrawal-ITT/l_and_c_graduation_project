import { assert } from 'chai';
import * as mocha from 'mocha';
import TopicManager from '../../Database/DbLayer/TopicManager';

describe('topic db layer tests for get messages', () => {
	let topicManager;
	beforeEach(() => {
		topicManager = new TopicManager();
	});

	it('should add topics in db', async (done) => {
		topicManager
			.addTopics()
			.then((response) => {
				assert.equal(response, 'TOPICS ADDED');
			})
			.catch((error) => {
				assert.equal(error.message, 'no messages exists');
			});
		done();
	});

	it('should verify topic', () => {
		let response = topicManager.verifyTopic('books');
		assert.equal(true, response);
	});
});
