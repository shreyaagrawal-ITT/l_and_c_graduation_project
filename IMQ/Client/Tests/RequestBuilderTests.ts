import { assert, use } from 'chai';
import * as mocha from 'mocha';
import { Actions, ClientRoles, Versions } from '../../Protocol/Enum';
import IMQprotocol from '../../Protocol/IMQProtocol';
import RequestBuilder from '../Request/RequestBuilder';

describe('requests builder test cases', () => {
	let requestBuilder: RequestBuilder;
	beforeEach(() => {
		requestBuilder = new RequestBuilder();
	});

	it('should generate registration request', () => {
		let user = {
			userName: 'Shreya',
			password: '12345',
			role: ClientRoles.publisher
		};
		let response = requestBuilder.generateRegistertionRequest(user.userName, user.password, user.role);
		assert.equal(user.role, response.role);
		assert.equal('register', response.action);
		assert.equal(user.userName, JSON.parse(response.data).username);
		assert.equal(user.password, JSON.parse(response.data).password);
	});

	it('should generate login request', (done) => {
		let user = {
			userName: 'Shreya',
			password: '12345',
			role: ClientRoles.publisher
		};
		let response = requestBuilder.generateLoginRequest(user.userName, user.password, user.role);
		assert.equal(user.role, response.role);
		assert.equal('login', response.action);
		assert.equal(user.userName, JSON.parse(response.data).username);
		assert.equal(user.password, JSON.parse(response.data).password);
		done();
	});

	it('should generate login request', () => {
		let user = {
			userName: 'Shreya',
			password: '12345',
			role: ClientRoles.publisher
		};
		let response = requestBuilder.generateLoginRequest(user.userName, user.password, user.role);
		assert.equal(user.role, response.role);
		assert.equal('login', response.action);
		assert.equal(user.userName, JSON.parse(response.data).username);
		assert.equal(user.password, JSON.parse(response.data).password);
	});

	it('should generate connect to topic request', () => {
		let data: IMQprotocol = {
			role: ClientRoles.publisher,
			action: Actions.connectTopic,
			data: '',
			version: Versions['IMQ1.1']
		};
		let topicName = 'books';
		let response = requestBuilder.generateConnectToTopicRequest(topicName, data);
		assert.equal(data.role, response.role);
		assert.equal('connect Topic', response.action);
		assert.equal(topicName, response.data);
		assert.equal('IMQ1.1', response.version);
	});

	it('should generate push request', () => {
		let data: IMQprotocol = {
			role: ClientRoles.publisher,
			action: Actions.connectTopic,
			data: 'books',
			version: Versions['IMQ1.1']
		};
		let message = 'book 1';
		let response = requestBuilder.generatePushRequest(message, data);
		assert.equal(data.role, response.role);
		assert.equal('push message', response.action);
		assert.equal(JSON.stringify({ 'topic': data.data, 'message': message }), response.data);
		assert.equal('IMQ1.1', response.version);
	});

	it('should generate pull request', () => {
		let data: IMQprotocol = {
			role: ClientRoles.publisher,
			action: Actions.connectTopic,
			data: '',
			version: Versions['IMQ1.1']
		};
		let topicName = 'books';
		let response = requestBuilder.generatePullRequest(topicName, data);
		assert.equal(data.role, response.role);
		assert.equal('pull message', response.action);
		assert.equal(topicName, response.data);
		assert.equal('IMQ1.1', response.version);
	});
});
