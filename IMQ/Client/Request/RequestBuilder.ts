import { Actions, Versions, ClientRoles } from '../../Protocol/Enum';
import IMQprotocol from '../../Protocol/IMQProtocol';

export default class RequestBuilder {
	request: IMQprotocol;

	constructor() {}

	public generateRegistertionRequest(username: string, password: string, role: ClientRoles) {
		let clientDetails = {
			'username': username,
			'password': password
		};
		this.request = {
			role: role,
			action: Actions.register,
			data: JSON.stringify(clientDetails),
			version: Versions['IMQ1.1']
		};

		return this.request;
	}

	public generateLoginRequest(username: string, password: string, role: ClientRoles) {
		let clientDetails = {
			'username': username,
			'password': password
		};
		this.request = {
			role: role,
			action: Actions.login,
			data: JSON.stringify(clientDetails),
			version: Versions['IMQ1.1']
		};

		return this.request;
	}

	public generateConnectToTopicRequest(topicName: string, data: IMQprotocol) {
		this.request = {
			role: data.role,
			action: Actions.connectTopic,
			data: topicName,
			version: Versions['IMQ1.1']
		};

		return this.request;
	}

	public generatePushRequest(message: string, data: IMQprotocol) {
		const messageObject = {
			topic: data.data,
			message: message
		};
		this.request = {
			role: data.role,
			action: Actions.push,
			data: JSON.stringify(messageObject),
			version: Versions['IMQ1.1']
		};

		return this.request;
	}

	public generatePullRequest(topic: string, data: IMQprotocol) {
		this.request = {
			role: data.role,
			action: Actions.pull,
			data: topic,
			version: Versions['IMQ1.1']
		};

		return this.request;
	}
}
