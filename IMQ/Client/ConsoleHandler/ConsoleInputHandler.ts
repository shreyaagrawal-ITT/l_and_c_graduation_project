import IMQprotocol from '../../Protocol/IMQProtocol';
import Logger from './Logger';
import * as net from 'net';
import ConnectionHandler from '../Connection/ConnectionHandler';
import PublisherController from '../Role/Publisher/PublisherController';
import SubscriberController from '../Role/Subscriber/SubscriberController';

export default class ConsoleInputHandler {
	logger = new Logger();
	private client: net.Socket;
	private connectionHandler: ConnectionHandler;
	private publisherController: PublisherController;
	private subscriberController: SubscriberController;

	constructor(socket: net.Socket) {
		this.client = socket;
		this.connectionHandler = new ConnectionHandler(this.client);
		this.publisherController = new PublisherController(this.client);
		this.subscriberController = new SubscriberController(this.client);
	}

	public processConsoleInput(request: IMQprotocol, input: string) {
		let command = input.split(' ');
		if (command[0] != 'imq') {
			this.logger.log('INVALID COMMAND');
		} else {
			switch (command[1]) {
				case 'connect': {
					this.connectionHandler.connectToTopic(command[2], request);
					break;
				}

				case 'publish': {
					let message = '';
					for (let i = 2; i < command.length; i++) {
						message = message + command[i] + ' ';
					}
					this.publisherController.publishMessage(message, request);
					break;
				}

				case 'subscribe': {
					this.subscriberController.subscribe(command[2], request);
				}
			}
		}
	}
}
