import * as readline from 'readline';

export default class ConsoleInput {
	public createConsoleInterface() {
		const consoleInput = readline.createInterface({ input: process.stdin, output: process.stdout, terminal: false });
		return consoleInput;
	}
}
