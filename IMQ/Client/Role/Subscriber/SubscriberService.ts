import * as net from 'net';
import RequestBuilder from '../../Request/RequestBuilder';
import { ClientRoles } from '../../../Protocol/Enum';
import IMQprotocol from '../../../Protocol/IMQProtocol';

export default class SubscriberService {
	private client;
	requestBuilder = new RequestBuilder();
	constructor(client: net.Socket) {
		this.client = client;
	}

	public registerSubscriber(username: string, password: string) {
		let requestPacket = this.requestBuilder.generateRegistertionRequest(username, password, ClientRoles.subscriber);
		this.client.write(JSON.stringify(requestPacket));
	}

	public loginSubscriber(username: string, password: string) {
		let requestPacket = this.requestBuilder.generateLoginRequest(username, password, ClientRoles.subscriber);
		this.client.write(JSON.stringify(requestPacket));
	}

	public pullMessages(topicsName: string, request: IMQprotocol) {
		let requestPacket = this.requestBuilder.generatePullRequest(topicsName, request);
		this.client.write(JSON.stringify(requestPacket));
	}
}
