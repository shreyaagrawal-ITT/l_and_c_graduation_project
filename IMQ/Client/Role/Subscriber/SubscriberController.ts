import * as net from 'net';
import IMQprotocol from '../../../Protocol/IMQProtocol';
import SubscriberService from './SubscriberService';

export default class SubscriberController {
	private client;
	private subscriberService: SubscriberService;

	constructor(client: net.Socket) {
		this.client = client;
		this.subscriberService = new SubscriberService(this.client);
	}

	public register(subscriberInfo: string[]) {
		this.subscriberService.registerSubscriber(subscriberInfo[1], subscriberInfo[2]);
	}

	public login(subscriberInfo: string[]) {
		this.subscriberService.loginSubscriber(subscriberInfo[1], subscriberInfo[2]);
	}

	public subscribe(topic: string, request: IMQprotocol) {
		this.subscriberService.pullMessages(topic, request);
	}
}
