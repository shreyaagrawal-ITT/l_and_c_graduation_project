import PublisherService from './PublisherService';
import * as net from 'net';
import IMQprotocol from '../../../Protocol/IMQProtocol';

export default class PublisherController {
	private client: net.Socket;
	private publisherService: PublisherService;

	constructor(client: net.Socket) {
		this.client = client;
		this.publisherService = new PublisherService(this.client);
	}

	public register(publisherInfo: string[]) {
		this.publisherService.registerPublisher(publisherInfo[1], publisherInfo[2]);
	}

	public login(publisherInfo: string[]) {
		this.publisherService.loginPublisher(publisherInfo[1], publisherInfo[2]);
	}

	public publishMessage(message: string, request: IMQprotocol) {
		this.publisherService.publishMessageToIMQ(message, request);
	}
}
