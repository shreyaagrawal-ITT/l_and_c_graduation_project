import * as net from 'net';
import RequestBuilder from '../../Request/RequestBuilder';
import { ClientRoles } from '../../../Protocol/Enum';
import IMQprotocol from '../../../Protocol/IMQProtocol';
import Logger from '../../ConsoleHandler/Logger';

export default class PublisherService {
	private client: net.Socket;
	requestBuilder = new RequestBuilder();
	logger = new Logger();
	constructor(client: net.Socket) {
		this.client = client;
	}

	public registerPublisher(username: string, password: string) {
		let requestPacket = this.requestBuilder.generateRegistertionRequest(username, password, ClientRoles.publisher);
		this.client.write(JSON.stringify(requestPacket));
	}

	public loginPublisher(username: string, password: string) {
		let requestPacket = this.requestBuilder.generateLoginRequest(username, password, ClientRoles.publisher);
		this.client.write(JSON.stringify(requestPacket));
	}

	public publishMessageToIMQ(message: string, data: IMQprotocol) {
		let isUserPublisher = this.validateUserRole(data.role, ClientRoles.publisher);
		if (isUserPublisher) {
			if (data['data'][0] != '{') {
				let requestPacket = this.requestBuilder.generatePushRequest(message, data);
				this.client.write(JSON.stringify(requestPacket));
			} else {
				data['data'] = JSON.parse(data['data'])['topic'];
				let requestPacket = this.requestBuilder.generatePushRequest(message, data);
				this.client.write(JSON.stringify(requestPacket));
			}
		} else {
			this.logger.log("You don't have permission to publish.Enter valid command");
		}
	}

	public validateUserRole(actualRole: ClientRoles, expectedRole: ClientRoles) {
		return actualRole == expectedRole;
	}
}
