import PublisherController from './Publisher/PublisherController';
import SubscriberController from './Subscriber/SubscriberController';
import * as net from 'net';

export default class ClientHandler {
	private publisherController: PublisherController;
	private subscriberController: SubscriberController;
	private clientSocket: net.Socket;

	constructor(socket: net.Socket) {
		this.clientSocket = socket;
		this.publisherController = new PublisherController(this.clientSocket);
		this.subscriberController = new SubscriberController(this.clientSocket);
	}

	public handleClientRole(clientDetails: string[]) {
		switch (clientDetails[0]) {
			case 'register_publisher':
				this.publisherController.register(clientDetails);
				break;

			case 'register_subscriber':
				this.subscriberController.register(clientDetails);
				break;

			case 'login_publisher':
				this.publisherController.login(clientDetails);
				break;

			case 'login_subscriber':
				this.subscriberController.login(clientDetails);
				break;
		}
	}
}
