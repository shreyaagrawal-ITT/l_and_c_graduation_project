import IMQprotocol from '../../Protocol/IMQProtocol';
import RequestBuilder from '../Request/RequestBuilder';
import * as net from 'net';

export default class ConnectionHandler {
	requestBuilder = new RequestBuilder();
	clientSocket: net.Socket;

	constructor(socket: net.Socket) {
		this.clientSocket = socket;
	}

	connectToTopic(topicName: string, data: IMQprotocol) {
		const request: IMQprotocol = this.requestBuilder.generateConnectToTopicRequest(topicName, data);
		this.clientSocket.write(JSON.stringify(request));
	}
}
