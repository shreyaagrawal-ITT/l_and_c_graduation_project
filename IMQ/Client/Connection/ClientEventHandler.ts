import * as net from 'net';
import Logger from '../ConsoleHandler/Logger';
import ResponseHandler from '../Response/ResponseHandler';
import ConsoleInputHandler from '../ConsoleHandler/ConsoleInputHandler';

export default class ClientEventHandler {
	private clientSocket: net.Socket;
	private logger: Logger;
	private consoleInputHandler: ConsoleInputHandler;
	private responseHandler: ResponseHandler;

	constructor(socket: net.Socket) {
		this.clientSocket = socket;
		this.logger = new Logger();
		this.responseHandler = new ResponseHandler(this.clientSocket);
		this.consoleInputHandler = new ConsoleInputHandler(this.clientSocket);
		this.clientListner();
	}

	private clientListner() {
		this.clientSocket.on('data', (data) => {
			this.responseHandler.processResponse(data.toString('utf-8'));
		});
		this.clientSocket.on('error', (error) => this.logger.log('SERVER DISCONNECTED: ' + error.message));
	}
}
