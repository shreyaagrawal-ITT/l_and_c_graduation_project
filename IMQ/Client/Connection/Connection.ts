#!/usr/bin/env node
import * as net from 'net';
import ClientEventHandler from './ClientEventHandler.js';
import { PORT } from '../Constants.js';
import ClientRoleHandler from '../Role/ClientRoleHandler';

class Connection {
	private clientRoleHandler: ClientRoleHandler;
	public clientEventHandler: ClientEventHandler;
	public clientSocket: net.Socket;

	constructor() {
		this.clientSocket = new net.Socket();
		this.clientRoleHandler = new ClientRoleHandler(this.clientSocket);
		this.clientEventHandler = new ClientEventHandler(this.clientSocket);
		this.connectClient(PORT);
		this.clientRoleHandler.handleClientRole(process.argv.splice(2));
	}

	private connectClient(connectionPort: number) {
		this.clientSocket.connect(connectionPort);
	}
}

let connection = new Connection();
