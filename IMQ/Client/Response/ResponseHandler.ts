import * as net from 'net';
import Logger from '../ConsoleHandler/Logger';
import ConsoleInput from '../ConsoleHandler/ConsoleInput';
import ConsoleInputHandler from '../ConsoleHandler/ConsoleInputHandler';
import { Actions } from '../../Protocol/Enum';

export default class RequestHandler {
	private client: net.Socket;
	logger = new Logger();
	private consoleInputHandler: ConsoleInputHandler;
	private consoleInput: ConsoleInput;
	constructor(socket: net.Socket) {
		this.client = socket;
		this.consoleInputHandler = new ConsoleInputHandler(this.client);
		this.consoleInput = new ConsoleInput();
	}

	public processResponse(data: string) {
		const readline = this.consoleInput.createConsoleInterface();
		let response = JSON.parse(data);

		if (
			JSON.parse(response).data == 'LOGGED IN' ||
			JSON.parse(response).data == 'REGISTERED' ||
			(JSON.parse(response).action == Actions.connectTopic && JSON.parse(response).data != null) ||
			JSON.parse(response).data == 'MESSAGE PUBLISHED' ||
			JSON.parse(response).action == Actions.push
		) {
			this.logger.log(`${JSON.parse(response).action} successful!! Enter next command:`);
			readline.on('line', (input) => {
				this.consoleInputHandler.processConsoleInput(JSON.parse(response), input);
				readline.close();
			});
		} else if (JSON.parse(response).data == 'NOT REGISTERED') {
			this.logger.log('Sorry! you are not registered.');
			readline.close();
			this.client.end();
		} else if (JSON.parse(response).action == Actions.connectTopic && JSON.parse(response).data == null) {
			this.logger.log('Please enter valid topic:');
		} else if (JSON.parse(response).action == Actions.pull) {
			this.logger.log(JSON.parse(response).data);
			readline.on('line', (input) => {
				this.consoleInputHandler.processConsoleInput(JSON.parse(response), input);
				readline.close();
			});
		}
	}
}
