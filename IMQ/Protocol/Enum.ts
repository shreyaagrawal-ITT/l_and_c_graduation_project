export enum Actions {
	register = 'register',
	push = 'push message',
	pull = 'pull message',
	login = 'login',
	deleteTopic = 'delete Topic',
	connectTopic = 'connect Topic'
}

export enum ClientRoles {
	publisher = 'publisher',
	subscriber = 'subscriber'
}

export enum Versions {
	'IMQ1.1' = 'IMQ1.1'
}
