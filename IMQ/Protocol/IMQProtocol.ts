import { Actions, Versions, ClientRoles } from './Enum';

export default interface IMQprotocol {
	role: ClientRoles;
	action: Actions;
	data: string;
	version: Versions;
}
